.\" Hey Emacs! This file is -*- nroff -*- source.
.\"
.\" Created for Debian using the original file "sig.announce.1.4"
.TH fits "3tcl" "2.3 Tcl" "Tcl interface to FITS Files"
.SH NAME
fits \- Tcl interface to FITS Files
.SH SYNOPSIS
\fBfits\fI (open|info|close)
.SH "Handling FitsFile objects"
.SS fits open fileName mode ?objName? 
Create a FitsFile object, open the file and read the header of the current
HDU. (Read the CFITSIO web pages or documentation to learn about its "Extended
File Name Syntax".) mode can be 0 (read only) or 1 (read and write). If no
objName is given, a name will be automatically assigned. A new empty FITS file
can be created by setting mode to 2. In this case, if the named file exists,
then it will be overwritten. Returns objName which will be used in most of the
methods as the handle to the object. 
.SS  fits info ?objName ...?
Return a list of all the existing objects, or the named objects (supports
regular expressions), with the following information: 
{objName fileName mode CHDU hduType}
where CHDU is the current HDU (1-primary array, 2-the first extension ...) and
hduType is the type of the HDU (0-image, 1-ASCII table, 2-Binary table).
.SS objName move ?+/-?n
If n has ``+/-'' sign, then move n units relative to the CHDU, otherwise move
to the nth HDU. Return the extension type of the new HDU: 0-image, 1-ASCII
table, 2-Binary Table.
.SS objName close
Delete object objName.
.SS fits close
Delete all the existing objects.
.SH "Getting information about the CHDU"
.SS objName info chdu
Return the CHDU. 1=primary, 2=first extension, etc. 
.SS objName info filesize
Return the size of the file (in units of 2880 bytes). 
.SS objName info hdutype
Return ``Primary array'', ``Image extension'', ``Binary Table'', or ``ASCII
Table''.
.SS objName info imgdim
Return the dimensions of the image if the CHDU is an image extension. 
.SS objName info ncols
Return the number of columns in the table if the CHDU is a table extension. 
.SS objName info nrows
Return the number of rows in the table if the CHDU is a table extension. 
.SS objName info nkwds
Return the number of keywords in the header of the CHDU. 
.SS objName info column ?\-exact? ?colList?
Without any arguments, return a list of all the column names in the table if
the CHDU is a table extension. If colList is present, detailed information
about the listed columns, using regular expression name comparisons, is
returned in the form of:

    {Name Type Unit DisplayFormat DefaultFormat ColumnWidth isOffset isScaled defaultNull}
    where
      	Name 	name of the column (TTYPE keyword)
    Type 	type of the column (TFORM keyword)
    Unit 	unit of the column (TUNIT keyword)
    DisplayFormat 	format for display (TDISP keyword)
    DefaultFormat 	default format for display (if TDISP keyword is absent)
    ColumnWidth 	the width of the column (in units of characters).
    isOffset 	0 = no offset (no TZERO keyword), 1 = offset.
    isScaled 	0 = not scaled (no TSCALE keyword), 1 = scaled.
    defaultNull 	default NULL value (TNULL keyword)
    The \-exact option turns off the regular expression matching. 

.SH "Reading data from a FITS file"
.SS objName dump ?\-s/\-e/\-l?
Return all the keyword records in the current header. The following options
control how the information is formatted: 
      	none 	return list of all ards in header
    \-s 	return three lists: {keywords} {values} {comments}
    \-e 	return a single string containing a newline-separated list of all header records
    \-l 	return all the keyword names
.SS objName get keyword ?keyList?
Return a list of {keyword value comment} for all the keywords in keyList
(supports regular expressions). If no keyList is given, return a list of
entries for all the keywords in the CHDU. The order in which keywords are
listed is undefined. (HISTORY and COMMENT keywords are never returned.)  
.SS objName get keyword \-num n
Return a list of {keyword value comment} of the nth keyword. 
.SS objName get wcs ?RAcol DecCol?
Return a list of the WCS parameters -- {xrval yrval xrpix yrpix xinc yinc rot
ctype} -- for the CHDU. If the current HDU is a table, supply the RAcol and
DecCol parameters specifying the two columns (as names or column numbers)
containing WCS data. Use defaults for any missing WCS keywords: 1.0 for
xinc/yinc; ``none'' for ctype; and 0.0 or all others. If the RA/Dec identity
of the columns (or image axes) are not known, an extra WCS parameter --
wcsSwap -- can be obtained which, when on, indicates that the X parameters are
for Dec instead of RA. Turn this option on with fits option wcsSwap 1.  
.SS objName get wcs \-m ?Col1 ...?
Similar to above, but information is returned in new-style format, allowing
arbitrary matrix transforms. Return value is {refVals refPix matrix types
projections} where each item (except matrix) contains a list of the given
information for each image dimension or table column; matrix is the rotation,
scale, skew, NxN transformation matrix given as {cd11 .. cd1N
cd21 .. cd2N .. cdNN}. The wcsSwap option has no effect on this command; one
must check the types values to see if the RA and Dec transforms are backwards.
.SS objName get image ?firstElem? ?numElem?
Read data elements from the current IMAGE extension and return them in a
list. Without any parameters, the entire image will be returned. If only
firstElem is given, return just one element.  
.SS objName get table ?\-c? ?\-noformat? ?colList? ?rows?
Read a block of a table and return list(s) of data. The block range is set in
      	colList 	a list of all the columns one wants to read. Read all columns if missing or is ``*''.
    rows 	a comma-separated list of row ranges of the form start-end (eg, ``3-5,7,9-'')
    \-c 	return data from each column as a separate list
    \-noformat 	do not format results according to a TDISPn, or default, format
.SS objName get vtable ?\-noformat? colName n ?rows?
Return a list of data in the nth vector element in column colName of the given rows. The \-noformat option turns off any TDISPn formatting. 
.SH "Loading data into memory or to a TCL array"
When loading data into memory, fitsTcl will normally return address dataType numElements where these are:
  	address 	Memory address of the data. Can be recovered in C by
  sscanf(address, "%p", &databuff);
with void *databuff
dataType 	
		
0 - BYTE_DATA 	1 byte data
1 - SHORTINT_DATA 	2 byte integer
2 - INT_DATA 	4 byte integer
3 - FLOAT_DATA 	4 byte floating point
4 - DOUBLE_DATA 	8 byte floating point
numElement 	Number of data elements in the array

.SS objName load image ?slice? ?rotate?
Load a full 2D image in the CHDU into memory and return the address address
(see above). slice indicates which frame to use if image is 3D. rotate
indicates how many 90-degree counter-clockwise rotations to perform on the
image (0-3).  
.SS objName load iblock arrayName firstRow numRows firstCol numCols ?slice?
Load a block (start from firstCol, firstRow with size numCols x numRows) of
image data to a 2-D TCL array arrayName or to memory if arrayName is
``--''. The indices of the array variable are
(firstCol-1 ... firstCol+numCols-1, firstRow-1 ... firstRow+numRows-1). A 1D
image will be treated as either a single row or column, depending on the
row/column parameters supplied. For a 3D image, slice indicates which frame to
use. If arrayName is ``--'', read the data block into memory and return the
pointer information: address dataType numElements. 
.SS objName load irows firstRow lastRow ?slice?
.SS objName load icols firstCol lastCol ?slice?
Read and average together a range of rows or columns of an image. Returns
address dataType numElements. dataType will be 3 for all image data types
except for double for which dataType is 4.  
.SS objName load column colName ?defaultNull? ?firstElement?
Load a column of a table into memory and return address dataType
numElements. Use the value of defaultNull for NULL values, or internal
defaults (normally the data type's maximum value) if ``NULL'' or absent. If
colName is a vector column, read the firstElement-th element. One can only
load numerical columns.  
.SS objName load vtable colName
Load all elements of the vector column colName and return address dataType
numElements.  
.SS objName load tblock ?\-noformat? arrayName colList firstRow numRows colIndex ?firstElem?
Load a block of table data to a 2-D TCL array arrayName. colIndex is (almost)
the column index of the array to use for the first column read. The first data
index is actually (as for images) (colIndex-1,firstRow-1). For scaler columns,
firstElem = 1. Without the \-noformat flag, values will be returned as strings
formatted according to a TDISP keyword or default format based on data type.
.SS objName load expr ?\-rows rows? expression ?defaultNull?
Evaluate the arithmetic expression on each row (or a subset of rows given by
rows) of the CHDU's table and return address dataType numElements, using
defaultNull as the null value of any NULL results.
.SS objName load keyword
Load keywords into fitsTcl's internal hash table. One usually will not need to
call this routine.
.SS objName load chdu
Reload the information about the current HDU. One usually will not need to
call this routine.
.SH "Writing to a FITS file"
.SS objName insert image bitpix naxis naxesList
.SS objName insert image \-p ?bitpix naxis naxesList?
Insert an empty image HDU (primary header with \-p) after the current HDU (or,
at start of file for primary array). The image parameters -- bitpix naxis
naxesList -- can be left off if creating a zero-length primary array.
.SS objName insert table numRows {colNames} {colFormats} ?{colUnits} extensionName?
.SS objName insert table \-ascii numRows {colNames} {colFormats} ?{colUnits} {colPosition} extensionName rowWidth?
Insert an empty BINARY (default) or ASCII (with \-ascii flag) table HDU after
the current HDU. The colNames and colFormats lists must be of equal length
(possibly both empty). The optional parameters can each be empty if one wants
to use default/empty values. 
.SS objName insert keyword index record ?formatFlag?
Insert a new record at the index-th keyword. If formatFlag is 0, write the
record exactly as passed, otherwise parse it as a free form keyname value
comment and reformat it into a standardized KEYNAME = VALUE / COMMENT
(default).
.SS objName put keyword ?\-num index? record ?formatFlag?
Write a keyword record either at position index; the position of a
pre-existing keyword with the same name; or append it if no such keyword
exists. See above for format/meaning of record and formatFlag.
.SS objName put history historyStr
Write a HISTORY keyword with content historyStr 
.SS objName insert column index colName colFormat
Insert an empty column with format colFormat before the index-th
column. colFormat specifies the column format as, for example: 
      ASCII Table: A15, I10, E12.5, D20.10, F14.6 ...
      BINARY Table: 15A, 1I, 1J, 1E, 1D, 1L, 1X, 1B, 1C, 1M 
.SS objName add column colName colFormat ?expression?
Without an expression, append an empty column to the CHDU table; return
nothing. Given an arithmetic expression, though, the indicated column
(new or old) will be filled in with the results of the expression
evaluated for each row of the table. If colName does not exist in the
current table extension, a new one will be created with format
colFormat (see above). If colFormat is ``default'', the column will be
created based on the data type of the result. Return 1 or 0 to
indicate whether a new column was created (1) or not (0). expression
can be in either C or Fortran format with table columns and keywords
referenced by their names. Here are some expression samples: 
      	Expression 	Result 	 
    17.2 	17.2 for every row
    17 + 4*(PHI > 32) 	17 or 21, depending on whether that row of the PHI column is greater than 32
    sqrt( (X\-X0)^2 + (Y\-Y0)^2 ) 	Distance of the (X,Y) column coordinates from the (X0,Y0) keyword values
See the CFITSIO web pages and documentation for more details. 
.SS objName insert row index numRows
Insert number of numRows rows after the index-th row. 
.SS objName add row numRows
Add numRows rows at the end of the CHDU table. 
.SS objName put image firstElem listOfData
Write a block of data to the CHDU image. The first pixel of an image has
firstElem = 1, not zero.
.SS objName put table colName firstElem rowSpan listOfData
Write a list of data to the firstElem-th element of column colName in the CHDU
table. (For scalar columns firstElem is 1.) rowSpan is a single row range of
form start-end with ``\-'' indicating all rows.
.SH "Deleting data from a FITS file"
.SS objName delete keyword keyList
Delete listed keywords, where the keyList can be the mix of keyword names and
index numbers. Keywords are deleted individually in sequence, causing keyword
positions to change after each deletion, so be careful when deleting multiple
keywords by index.
.SS objName delete cols colList
Delete listed columns in a table extension. 
.SS objName delete rows firstRow numRows
Delete a block of rows. 
.SS objName delete rows \-expr expression
Delete rows using expression which must evaluate to a boolean value. Rows for
which expression evaluates to TRUE get deleted.
.SS objName delete chdu
Delete the current HDU. The HDU immediately following the one deleted (or the
preceding one if the current HDU is the last one of the file) will become the
new current HDU. Returns extension type of new HDU: 0-image, 1-ASCII table,
2-Binary Table.
.SH "Analyzing FITS data"
.SS objName sort ?\-merge? colList ?ascendFlags?
Sort table rows using columns in colList. When \-merge is present, if multiple
rows have identical sort keys all but one of the rows will be deleted. If
present, ascendFlags contains a list of 1s and 0s indicating whether that
column will be sorted in ascending (1, the default) or descending (0) order.
.SS objName column \-stat colName ?firstElem? ?rows?
Return statistics on the firstElem-th element (1 for scalar columns) of column
colName in the order min firstMinRow max firstMaxRow mean stdDev numData.
.SS objName column \-minmax colName ?firstElem? ?rows?
Returns minimum and maximum values of the firstElem-th element (1 for scalar
columns) of column colName.
.SS objName histogram ?\-weight colName|value? ?\-inverse? ?\-rows rows? filename {binAxis1} ?{binAxis2} ...?
Create a 1D - 4D histogram from columns in the current table. The binning
parameters are given by the binAxisN parameters which are lists of the form
colName min max binSize where the last 3 elements can each be ``\-'',
indicating default values. If TLMINn, TLMAXn, and/or TDBINn exist, those
values will be used for defaults. Otherwise, for min and max the defaults are
the actual min/max values of the data; for binSize the smaller of one-tenth
the data span or 1.0 will be selected. A weighting value can be indicated with
the \-weight option. This can be either another column or a numerical
constant. The \-inverse option indicates the weight should be 1.0/weight
instead of the weighting value itself. The histogram can be restricted to
certain row ranges using the \-rows option.
.SS objName smooth {width height} outfile ?inPrimary?
Smooth the current image extension with a boxcar function of dimensions width
by height pixels. The dimensions must be odd integers. The resulting image
will be placed in a new extension appended to the file outfile. If outfile
does not exist, the image will be placed either in the primary or in the first
extension, depending on the value of the inPrimary flag; the default is to
place it in an extension.
.SH "List/Pointer Manipulation"
.SS lst2ptr dataList ?dataType? ?naxesList?
Convert a TCL list into a memory-resident array. Returns address dataType
naxesList (see the load commands above). The TCL list will be cast as double
values in the absence of the dataType parameter. The parameter naxesList gives
the vector dimensions of the list. It can be a single number indicating the
length of the list (the default value when absent) or a list of numbers which
must multiply together to be the list length. If a list entry contains the
string "NULL", the maximum value of the given dataType will be inserted.
.SS ptr2lst address dataType naxesList
Convert a memory pointer into a TCL list. Returns dataList dataType naxesList
(see lst2ptr). If an array element contains the maximum value for the given
dataType, the string "NULL" will be inserted into the list.
.SS vexpr ?\-ptr? ?\-use getDataCallback? expression
Perform a vector calculation using lists or arrays where expression is a
C-style arithmetic expression. (Ie, do not use $var notation unless you
explicitly want variable substitution before passing expression to vexpr.)
Without any of the options, variable references within expression will be
interpretted as local TCL lists (or scalars) containing double data and the
result will itself be a simple TCL list. With the \-ptr option set, the answer
instead will be returned as a pointer to a memory-resident array. With either
option set, two additional parameters will be returned in a list of the form
dataList dataType naxesList or address dataType naxesList. The \-use option
provides a "callback" routine which is to be used for supplying the variable
data. It gets called when a variable is encountered in the expression and will
be passed a single argument containing the name of the variable. The callback
routine should return either "dataList dataType naxesList" or "\-ptr address
dataType naxesList". If the callback routine doesn't recognize the variable,
it should return an empty string. The parser will then try to find the
variable in the local namespace, as when no callback is supplied. If this
fallback behavior is not desired, raise an error instead. The callback
function can itself check for local variables using the TCL command upvar 1
$varName localName. The following code snippet provides a sample callback
function which assumes the expression contains variables storing the results
of a load or lst2ptr command (ie, address dataType naxesList):
    proc getPtrData { varName } { upvar $varName myName if [info exists myName] { return [eval list \-ptr $myName] } else { error "Variable doesn't exist" } }
    then the following code would calculate the average value of a table column:
    set myCol [objName load column X] vexpr \-use getPtrData "sum( (double)myCol ) / nelem( myCol )"
By default, a vector with a single-valued naxesList is interpretted as a
single vector containing n elements such that the above "sum" function sums
all of the elements. In the context of loading a column, though, one actually
has an array of n rows with each row containing 1 (or perhaps more)
elements. To handle this case, one must use the callback function to provide
an naxisList of "1 n" (or "2 m", etc) instead of merely "n". In that case each
row will be calculated separately and have its own result. When naxesList has
multiple elements, the final element specifies the number of rows. 

See the CFITSIO web pages and documentation for details on expression
syntax. One can also read the fv online help file on expression syntax, noting
that vexpr can only handle numerical input and output.
.SS fits free addressList
Free the memory occupied by the data generated by one of the load, lst2ptr,
and vexpr commands.
.SH "Other commands"
.SS objName copy filename
Copy the CHDU to a new FITS file. If the CHDU is not an image array, then an
empty primary array is inserted.
.SS objName sascii table filename fileMode firstRow numRows colList widthList
Write the listed columns into an ASCII file. In the output file, each column
has the width listed in widthList. fileMode indicates how to write the data to
filename: 0-create new file and write column headings; 1-append to old file
and write column headings; 2-append to old file, but don't write any headings.
.SS objName sascii image filename fileMode firstRow numRows firstCol numCols cellSize ?slice?
Write a block of the image data to an ASCII file. In the output file, the data
has width of cellSize. Use frame slice for 3D image data. 
.SS objName flush ?clear?
Flush any dirty buffers to the disk. With clear, free the buffers, too. 
.SS objName append filename
Append current HDU to another FITS file. 
.SS objName checksum update|verify
Update or Verify the checksum keywords in the current HDU. If none exist,
update will create them. For verify, the return values are 1 for valid values,
0 if either keyword (CHECKSUM or DATASUM) is missing, and \-1 for invalid
values.
.SS objName info expr expression
Get information on the result of the supplied arithmetic expression. Return
value is cDataType numElements {naxesList} where cDataType is the CFITSIO
datatype (not fitsTcl's): TDOUBLE, TSTRING, etc; numElements is the number of
elements in the result; and naxesList contains the vector dimensions of the
result ({1} for scalar result). An expression which evaluates to a constant
(no dependency on the contents of the table) will be indicated by a negative
number of elements.
.SS range count rangeStr maxElem
Count the number of elements (esp. rows) contained within the comma-separated
list of ranges in rangeStr (eg, 1-4,5,7-). To support ranges of the form "7-"
which specifies a range from element 7 through the final element, maxElem is
passed with the maximum element number.
.SS fits option ?option? ?value?
Modify fitsTcl's default behavior. With no options, this returns a list of all
the current fitsTcl options and their values. With the option parameter,
return the value of that parameter. When both option and value are supplied,
set the parameter to the given value. Current options: wcsSwap (0).
.SS fits version
Get version numbers for fitsTcl and underlying cfitsio. 
.SH AUTHOR
Bryan Irby
