#!/usr/bin/tclsh
# This file is a -*- tcl -*- file

cd debian/tests
package require tcltest
tcltest::verbose {pass body error}
proc tcltest::cleanupTestsHook {} {
    variable numTests
    upvar 2 testFileFailures crashed
    set ::code [expr {$numTests(Failed) > 0}]
    if {[info exists crashed]} {
        set ::code [expr {$::code || [llength $crashed]}]
    }
}
tcltest::runAllTests
exit $code
